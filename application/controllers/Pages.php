<?php

class Pages extends CI_Controller {

    public function home($page = 'home')
    {
        if (!file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('form'); //helper pour les formulaires
        $this->load->library('form_validation'); //utilisation du formulaire
        $this->form_validation->set_error_delimiters('<div class="bulle">', '</div>'); //balise d'erreur
        $img_error = '<i class="material-icons i-bulle">&#xe000;</i>'; //image d'erreur
        /*
         *  contrainte des input
         *  @required : le inpu doit être remplis
         *  @max_length : la longueur maximale du champ
         *  @valid_email : verifie si l'email est valide
         */
        
        $this->form_validation->set_rules('name', 'name', 'required|max_length[30]',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
              'max_length' => $img_error.'<div class="bulle-content">The %s is too long.</div>' )); 
        
        $this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[100]',
            array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
                  'valid_email' => $img_error.'<div class="bulle-content">The %s field must be valid.</div>',
                  'max_length' => $img_error.'<div class="bulle-content">The %s field is too long.</div>'));    

        $this->form_validation->set_rules('phone', 'phone', 'required|max_length[10]',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
              'max_length' => $img_error.'<div class="bulle-content">The %s field is too long.</div>' )); 
        
        $this->form_validation->set_rules('message', 'message', 'required',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>'));
        
        $data['title'] = ucfirst($page); // Met en Capital la premiere lettre
        $data['url'] = "index.php/pages/".$page; //l'url de la page
        $data['success']='fail';
        
        
        $this->form_validation->run();
        
        if($this->form_validation->run())
        {
            $data['success']='valid';
            
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $message = $this->input->post('message');
           
            $this->load->library('email');

            $this->email->from($email, $name);
            $this->email->to('jerome.gallix@ffwlab.com');

            $this->email->subject("Atomik cars - Contact from $name");
            $this->email->message(
                "Message :

    $message


Phone : $phone");

            $this->email->send();
        }
        
        $this->load->view('templates/head',$data);
        $this->load->view('templates/header',$data);
        $this->load->view('pages/'.$page,$data);
        $this->load->view('templates/footer',$data);
        
    }
    
    
    public function atomik($page = 'atomik')
    {
        if (!file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('form'); //helper pour les formulaires
        $this->load->library('form_validation'); //utilisation du formulaire
        $this->form_validation->set_error_delimiters('<div class="bulle">', '</div>'); //balise d'erreur
        $img_error = '<i class="material-icons i-bulle">&#xe000;</i>'; //image d'erreur
        /*
         *  contrainte des input
         *  @required : le inpu doit être remplis
         *  @max_length : la longueur maximale du champ
         *  @valid_email : verifie si l'email est valide
         */
        
        $this->form_validation->set_rules('name', 'name', 'required|max_length[30]',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
              'max_length' => $img_error.'<div class="bulle-content">The %s is too long.</div>' )); 
        
        $this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[100]',
            array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
                  'valid_email' => $img_error.'<div class="bulle-content">The %s field must be valid.</div>',
                  'max_length' => $img_error.'<div class="bulle-content">The %s field is too long.</div>'));    

        $this->form_validation->set_rules('phone', 'phone', 'required|max_length[10]',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
              'max_length' => $img_error.'<div class="bulle-content">The %s field is too long.</div>' )); 
        
        $this->form_validation->set_rules('message', 'message', 'required',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>'));
        
        $data['title'] = ucfirst($page); // Met en Capital la premiere lettre
        $data['title'] = $data['title'].' 500';
        $data['url'] = "index.php/pages/".$page; //l'url de la page
       
        $this->form_validation->run();
        
        $this->load->helper('url_helper'); //helper pour les chemins de pages & images
        $this->load->helper('file'); //helper pour les fichers
        
        $path = (FCPATH."assets/img/500/gallery/");//Chemin des fichiers dans le repertoire gallery
        $data['lesPhotos']= get_filenames($path);//Recupère les nom de chaque fichiers du chemin path
        
        $data['success']='fail';
        
        
        $this->form_validation->run();
        
        if($this->form_validation->run())
        {
           $data['success']='valid';
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $message = $this->input->post('message');
           
            $this->load->library('email');

            $this->email->from($email, $name);
            $this->email->to('jerome.gallix@ffwlab.com');

            $this->email->subject("Atomik cars - Contact from $name");
            $this->email->message(
                "Message : $message

Phone : $phone");

            $this->email->send();
        }
        
        $this->load->view('templates/head',$data);
        $this->load->view('templates/header',$data);
        $this->load->view('pages/'.$page,$data);
        $this->load->view('templates/footer',$data);
        
    }
    
    
    public function services($page = 'services')
    {
        if (!file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        
        $this->load->helper('form'); //helper pour les formulaires
        $this->load->library('form_validation'); //utilisation du formulaire
        $this->form_validation->set_error_delimiters('<div class="bulle">', '</div>'); //balise d'erreur
        $img_error = '<i class="material-icons i-bulle">&#xe000;</i>'; //image d'erreur
        /*
         *  contrainte des input
         *  @required : le inpu doit être remplis
         *  @max_length : la longueur maximale du champ
         *  @valid_email : verifie si l'email est valide
         */
        
        $this->form_validation->set_rules('name', 'name', 'required|max_length[30]',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
              'max_length' => $img_error.'<div class="bulle-content">The %s is too long.</div>' )); 
        
        $this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[100]',
            array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
                  'valid_email' => $img_error.'<div class="bulle-content">The %s field must be valid.</div>',
                  'max_length' => $img_error.'<div class="bulle-content">The %s field is too long.</div>'));    

        $this->form_validation->set_rules('phone', 'phone', 'required|max_length[10]',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
              'max_length' => $img_error.'<div class="bulle-content">The %s field is too long.</div>' )); 
        
        $this->form_validation->set_rules('message', 'message', 'required',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>'));
        
        $data['title'] = ucfirst($page); // Met en Capital la premiere lettre
        
        $data['url'] = "index.php/pages/".$page; //l'url de la page
        
        $data['success']='fail';
        
        $this->form_validation->run();
        
        if($this->form_validation->run())
        {
           $data['success']='valid';
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $message = $this->input->post('message');
           
            $this->load->library('email');

            $this->email->from($email, $name);
            $this->email->to('jerome.gallix@ffwlab.com');

            $this->email->subject("Atomik cars - Contact from $name");
            $this->email->message(
                "Message : $message

Phone : $phone");

            $this->email->send();
        }
        
        $this->load->view('templates/head',$data);
        $this->load->view('templates/header',$data);
        $this->load->view('pages/'.$page,$data);
        $this->load->view('templates/footer',$data);
        
    }
    
    
    public function news($page = 'news')
    {
        if (!file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('form'); //helper pour les formulaires
        $this->load->library('form_validation'); //utilisation du formulaire
        $this->form_validation->set_error_delimiters('<div class="bulle">', '</div>'); //balise d'erreur
        $img_error = '<i class="material-icons i-bulle">&#xe000;</i>'; //image d'erreur
        /*
         *  contrainte des input
         *  @required : le inpu doit être remplis
         *  @max_length : la longueur maximale du champ
         *  @valid_email : verifie si l'email est valide
         */
        
        $this->form_validation->set_rules('name', 'name', 'required|max_length[30]',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
              'max_length' => $img_error.'<div class="bulle-content">The %s is too long.</div>' )); 
        
        $this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[100]',
            array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
                  'valid_email' => $img_error.'<div class="bulle-content">The %s field must be valid.</div>',
                  'max_length' => $img_error.'<div class="bulle-content">The %s field is too long.</div>'));    

        $this->form_validation->set_rules('phone', 'phone', 'required|max_length[10]',
        array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>',
              'max_length' => $img_error.'<div class="bulle-content">The %s field is too long.</div>' )); 
        
        $this->form_validation->set_rules('message', 'message', 'required',
            array('required' => $img_error.'<div class="bulle-content">The %s field is required.</div>'));
        
                
        $data['title'] = ucfirst($page); // Met en Capital la premiere lettre
        $data['url'] = "index.php/pages/".$page; //l'url de la page
        $data['success']='fail';
        
        
        $this->form_validation->run();
        
        if($this->form_validation->run())
        {
           $data['success']='valid';
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $message = $this->input->post('message');
           
            $this->load->library('email');

            $this->email->from($email, $name);
            $this->email->to('ruffinel.joshua@gmail.com');

            $this->email->subject("Atomik cars - Contact from $name");
            $this->email->message(
                "Message : $message

Phone : $phone");

            $this->email->send();
        }
        $this->load->view('templates/head',$data);
        $this->load->view('templates/header',$data);
        $this->load->view('pages/'.$page,$data);
        $this->load->view('templates/footer',$data);
    }
    
}
