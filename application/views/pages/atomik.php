<div class="container">
     <div class="home ">
        <div class="home-content">
            <h3 class="title">Atomik 500</h3>
            <div class="sub-title">Atomik 500 is a world premiere : the first electric city supercar.</div>
            <div class="b-text">
                Atomik 500 is a unique automotive synthesis combining concepts seen so far as antagonistic<br />
                - Supercar's perfomances in citycar size<br />
                - Fun to drive and environment-conscious
            </div>
            <div class="l-text">
                If you wish to be among the first ones to drive tomorrow's technologies, come aboard Atomik 500 :<br />
                high perfomrmance batteries without danger or pollution, 3-way electric network (including one high voltage),<br />
                ultracompact high efficiency transmissions, carbon / nomex rear chassis, ceramic heater.<br />
            </div>
            <div class="l-text">
                As exclusivity has to be noticed, we provide Atomik 500 a specific look accordingly to the electric propulsion requirements and inspired by the 1960s car racing universe.
            </div>
            <div class="l-text-last">
                Atomik 500 is availlable in sedan convertible versions.
            </div>
        </div>
        <div class="img-r">
            <img class="img-title el-desk" src="<?php echo base_url('assets/img/500/img-500-01.png'); ?>"/>
        </div>
    </div>
    <div class="specs">
        <div class="spec-title g-title">Specs</div>
        <div id="slider" class="slider ">
            <figure id="f-slide">
                <div class="slide slide-active" data-slide="0">
                    <div class="i-slide"><img class="img-slide" src="<?php echo base_url('assets/img/500/dimensions.png'); ?>"/></div>
                    <div class="info-slide regular">
                        <div class="title-slide">Dimensions</div>
                        <div class="desc-slide">
                            <ul>
                                <li class="d-slide">Weight 1.400 kg</li>
                                <li class="d-slide">Length 3m60</li>
                                <li class="d-slide">Height 1m46</li>
                                <li class="d-slide">Front tyres 205/55 - 13" 3 part wheels</li>
                                <li class="d-slide">Rear tyres 245/50 - 13" parts wheels</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="slide" data-slide="1">
                    <div class="i-slide"><img class="img-slide" src="<?php echo base_url('assets/img/500/performance.png'); ?>"/></div>
                    <div class="info-slide regular">
                        <div class="title-slide">Performance</div>
                        <div class="desc-slide">
                            <ul>
                                <li class="d-slide">0-100km/h - 0-665 mph < 5s 1000m D.A. 26 s</li>
                                <li class="d-slide">V max > 200km/h (> 121 mph) / electronic cut off: 180 km/h Range Up to 200 km (130 miles)</li>
                                <li class="d-slide">gCO2 / km 0</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="slide" data-slide="2">
                    <div class="i-slide"><img class="img-slide" src="<?php echo base_url('assets/img/500/engines.png'); ?>"/></div>
                    <div class="info-slide regular">
                        <div class="title-slide">Engines</div>
                        <div class="desc-slide">
                            <ul>
                                <li class="d-slide">Full Electronic hight power pulse width modulation,</li>
                                <li class="d-slide">variable frequency regenerative drive</li>
                                <li class="d-slide">- Front engine Power 110 kW</li>
                                <li class="d-slide">- Rear engine Power 110 kW</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="slide" data-slide="3">
                    <div class="i-slide"><img class="img-slide" src="<?php echo base_url('assets/img/500/transmission.png'); ?>"/></div>
                    <div class="info-slide regular">
                        <div class="title-slide">Transmission</div>
                        <div class="desc-slide">
                            <ul>
                                <li class="d-slide">Permanante 4 wheel driveFull eletric variable torque balance (front/rear)</li>
                                <li class="d-slide">Antsskid - ESP : Full electronic control</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="slide" data-slide="4">
                    <div class="i-slide"><img class="img-slide" src="<?php echo base_url('assets/img/500/batteries.png'); ?>"/></div>
                    <div class="info-slide regular">
                        <div class="title-slide">Batteries</div>
                        <div class="desc-slide">
                            <ul>
                                <li class="d-slide">On-board energy 37 kwh High voltage DC BUS Charging time </li>
                                <li class="d-slide">standard 220 V : 80% in 10h</li>
                                <li class="d-slide">Range optional 380 V : from 1h to 3h30 (according to electric plugging available)</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </figure>
            <div id="btn-left-slide" class="btn-left-slide btn-slide el-desk">&#10094;</div>
            <div id="btn-right-slide" class="btn-right-slide btn-slide el-desk">&#10095;</div>
        </div>
    </div>
    <div class="gallery">
        <div class="gallery-title g-title">Gallery</div>
        <div class="gallery-content">
            <div class="s-gallery">
                <?php
                    asort($lesPhotos);
                    foreach ($lesPhotos as $unePhoto){
                        $img =rtrim($unePhoto,".png"); //ex : galerie01
                        ?> 
                            <div class="i-gallery" data-modal="<?php echo $img; ?>">
                                <img class="img-gallery" src="<?php echo base_url('assets/img/500/gallery/'.$unePhoto); ?>"/>
                            </div>
                            
                        <?php
                    }
                ?>
            </div>
            <div id="modal" class="modal">
                
                <div class="modal-content">
                    <div class="modal-header">
                        <span id="close" class="close">&times;</span>
                    </div>
                    <div class="modal-body">
                        <img id="img-modal" class='img-modal' src="" />
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="btn-gallery regular"><a href="<?php echo base_url('assets/img/500/gallery.zip'); ?>" download='gallery'>Download hd photos of the cars</a></div>
    </div>
</div>

