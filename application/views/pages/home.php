
<div class="container container-home">
    <div class="home ">
        <div class="home-content">
            <h3 class="title">Atomik Cars</h3>
            <div class="sub-title">Atomik exclusivity trhu avant garde</div>
            <div class="b-text">
                Atomik Cars offers you the opportunity to aquire today technologies that will be standard for tomorrow's automobiles.
            </div>
            <div class="l-text">
                Anticipating the future is our motto.<br />
                We combine the results of our own R&D with the skills of world-class labs and specialists to offer you the most relevant innovation.<br />
                Our passion : inventing and producting real-world cars. <br />
                For over 20 years, the team gathered by J. Gallix has imagined, drawn, conceived an manufactured automobiles.
            </div>
            <img class="i-text" src="<?php echo base_url('assets/img/home/car-home.png'); ?>"/>
        </div>
        <div class="img-r">
            <img class="img-title el-desk" src="<?php echo base_url('assets/img/home/img-home-01.png'); ?>"/>
            <img class="img-title el-mobile" src="<?php echo base_url('assets/img/500/galerie-full-size/galerie-21.jpg'); ?>"/>
        </div>
    </div>
    <div class="content">
         <div class="m-img">
            <img class="img-mini" src="<?php echo base_url('assets/img/home/mini-home-01.png'); ?>"/>
            <img class="img-mini" src="<?php echo base_url('assets/img/home/mini-home-02.png'); ?>"/>
            <img class="img-mini" src="<?php echo base_url('assets/img/home/mini-home-03.png'); ?>"/>
            <img class="img-mini" src="<?php echo base_url('assets/img/home/mini-home-04.png'); ?>"/>
            <img class="img-mini" src="<?php echo base_url('assets/img/home/mini-home-05.png'); ?>"/>
            <img class="img-mini" src="<?php echo base_url('assets/img/home/mini-home-06.png'); ?>"/>
            <img class="img-mini" src="<?php echo base_url('assets/img/home/mini-home-07.png'); ?>"/>
        </div>
        <div class="img-l el-desk">
            <img class="img-title" src="<?php echo base_url('assets/img/home/img-home-02.png'); ?>"/>
        </div>
        <div class="main-content">
            <div class="text-content b-text">
                But the technologic archievement can't be satisfying per se; our quest of automobile excellence is global:
            </div>
            <div class="cont">
                <div class="cont-middle">
                    <div class="cont-m">
                        <div class="medium">Our manufacturing</div>
                        process focuses on a rigorous assembly and a flawless finish of every handcrafted copy.
                    </div>
                </div>
                <div class="cont-l">
                    <div class="cont-m">
                        <div class="medium">Each product</div>
                        comes with an exclusive style                    
                    </div>
                </div>
                <div class="cont-r">
                    <div class="cont-m">
                        <div class="medium">Perfomance</div>
                        must be exceptional in any field
                    </div>
                </div>
            </div>
            <div class="info-content semibold">
                If interest in acquiring Atomik's technology, please :
            </div>
            <div id="contact-content" class="contact-content el-desk"><a href="#menu">Contact us</a></div>
            <div class="contact-content contact-mobile el-mobile"><a href="#">Contact us</a></div>
        </div>
        <div class="img-l el-mobile">
            <img class="img-title" src="<?php echo base_url('assets/img/home/img-home-02.png'); ?>"/>
        </div>
    </div>
    <div class="ending regular">
        <div class="l-ending">
            <p>
                Atomik Cars is settled in Paris where we are permeated with the spirit of creativity and luxury which is recognized worldwide as a character of the French capital.<br />
                Wo focus on production systems like computers cleanrooms and equipped with cutting edge technologiy workshops.<br />
                Our experts use their skill and virtuosity to make, model, assemble every part and in the end every car in a unique process.<br />
                They combine art with tradition avant-garde.<br />
            </p>
        </div>
        <div class="r-ending">
            <p>A team of passionate automotive professionals, each with over 20 years of experience.</p>
            <p>Sharp skills in creating unique cars :  design, marketing, outstanding technology, prototypes assembly, racing.</p>
            <p>The integration or permanent availability of the most advanced means of designing, prototyping and testing.</p>
            <p class="italic i-end">Style creating starts making sense once the challenges of a technological breakthrough have been successfully adressed</p>
            <p class="e-ending">-J. Gallix</p>
        </div>
    </div>
    
</div>
    
