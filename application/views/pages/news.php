<div class="container">
    <div class="home ">
        <div class="home-content">
            <h3 class="title">News</h3>
            <div class="sub-title">The News of Atomik Cars, events & announcements.</div>
            <div class="b-text t-text">Salon Prive London 21-23 July 2010</div>
            <img class="img-text" src="<?php echo base_url('assets/img/actu01.png'); ?>">
            <div class="l-text c-text">
                Atomik Cars exhibits in Salon Prive London from 21st to 23rd July.
            </div>
            <a class="link-text l-text" href="https://www.salonpriveconcours.com">www.salonprivelondon.com</a>
            <div class="b-text t-text">Top Marques Monaco 15-18 April 2010</div>
            <img class="img-text" src="<?php echo base_url('assets/img/actu02.png'); ?>">
            <div class="l-text c-text">
                Atomik Cars reveals the brand new Atomik 500 at Top Marques Monaco.
            </div>
            
            
        </div>
        <div class="img-r">
            <img class="img-title block" src="<?php echo base_url('assets/img/img-news.png'); ?>"/>
        </div>
    </div>
</div>