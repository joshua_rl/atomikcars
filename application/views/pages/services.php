<div class="container">
    <div class="home ">
        <div class="home-content">
            <h3 class="title">Services</h3>
            <div class="sub-title">Technologies implemented for Atomik 500 are likely to be used in other EV transformations.</div>
            <div class="b-text">
                We will be pleased to examine your request and discuss with you about its realization.
            </div>
            <div class="l-text">
                If interest in acquiring Atomik's technology, please mail us at :
            </div>
            <div class="l-text mail semibold el-desk">sales@atomik-cars.com</div>
            <div class="home-contact regular contact-mobile el-mobile">contact us</div>
            <div class="l-text">
                All specific requirements regarding customization, equipments, materials or trims will be considered with greatest attention.<br />
                An other program covering perfomances, recharge, HiFi, luggage and other accessories is available on request.
            </div>
        </div>
        <div class="img-r">
            <img class="img-title block" src="<?php echo base_url('assets/img/img-services.png'); ?>"/>
        </div>
    </div>
</div>