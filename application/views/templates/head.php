<!DOCTYPE html>

<?php 
    $this->load->helper('url_helper'); //helper pour les url et liens
    $this->load->helper('form'); //helper pour les formulaires
?>

<html>
    <head>
        <title><?php echo "Atomik Cars - $title" ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/font/font.css" rel ="stylesheet">
        <link href="<?php echo base_url('assets/font/font.css'); ?>" rel ="stylesheet">
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel ="stylesheet">     
        <link href="<?php echo base_url('assets/css/mobile.css'); ?>" media="all and (max-width: 1280px)" rel ="stylesheet">
        <script src="<?php echo base_url('assets/js/jquery.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/fonction.js'); ?>" type="text/javascript"></script>
    </head>
    <body>
    