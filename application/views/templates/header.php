    
<?php 
    $this->load->helper('form'); //helper pour les formulaires
?>

<div id="contact-form-mobile" class="contact-form-mobile">
        <?php
            echo form_open($url, 'id=form-contact class="contact-form regular el-mobile" method="POST"'); 
        ?>
        <div class="btn-contact el-mobile">
            <i id="btn-close-contact" class="i-close-contact material-icons">&#xe5cd;</i>
        </div>
        <div id="form-content" class="form-content">
            <label>Name</label>
            <input class="regular" type="text" name="name" value="<?php echo set_value('name');?>"/>
            <?php echo form_error('name'); ?>
            <label>Email</label>
            <input class="regular" type="text" name="email" value="<?php echo set_value('email');?>"/>
            <?php echo form_error('email'); ?>
            <label>Phone</label>
            <input class="contact-inp regular" type="tel" name="phone" value="<?php echo set_value('phone');?>"/>
            <?php echo form_error('phone'); ?>
            <label>Message</label>
            <textarea class="regular" name="message"><?php echo set_value('message');?></textarea>
            <?php echo form_error('message'); ?>
            <input id="hid-mobile"  type="hidden" value="<?php echo set_value('hid-mobile');?>" name="hid-mobile" />
            <input id="btn-submit-mobile" class="light" type="submit" value="Send"/>
        </div>
        <?php
            if($success=='valid'){
                echo "
                    <div class='form-message'>
                        <p>Your request has been sent</p>
                        <p>Thank you !</p>
                    </div>
                ";
            }
        ?>
    </form>
</div>
<div id="body-content">
    <header>
        <nav id="menu" class="menu light">
            <div class="btn-open el-mobile">
                <i id="btn-close" class="i-close material-icons">&#xe5cd;</i>
            </div>
            <ul class="nav-menu">
                <li class="select" id="home"><a href="<?php echo site_url('index.php/pages/home'); ?>">Atomik Cars</a></li>
                <li class="select" id="atomik"><a href="<?php echo site_url('index.php/pages/atomik'); ?>">Atomik 500</a></li>
                <li class="select" id="service"><a href="<?php echo site_url('index.php/pages/services'); ?>">Services</a></li>
                <li class="select" id="news"><a href="<?php echo site_url('index.php/pages/news'); ?>">News</a></li>
                <li class="select el-mobile"><a class="contact-mobile" href="#">Contact</a></li>
            </ul>
        </nav>
        <?php
            echo form_open($url, 'id=form-contact class="contact-form regular el-desk" method="POST"'); 
        ?>
        
        <!--<form id="contact-form" method="POST" class="contact-form regular el-desk">-->
            <div id="contact" class="contact">Contact us</div>
            <br />
            <div id="form-content" class="form-content">
                <label>Name</label>
                <input id="name" class="regular" type="text" name="name" value="<?php echo set_value('name');?>"/>
                <?php echo form_error('name'); ?>
                <label>Email</label>
                <input id="email" class="regular" type="text" name="email" value="<?php echo set_value('email');?>"/>
                <?php echo form_error('email'); ?>
                <label>Phone</label>
                <input id="phone" class="contact-inp regular" type="tel" name="phone" value="<?php echo set_value('phone');?>"/>
                <?php echo form_error('phone'); ?>
                <label>Message</label>
                <textarea id="message" class="regular" name="message"><?php echo set_value('message');?></textarea>
                <?php echo form_error('message'); ?>
                <input id="hid-desk" type="hidden" value="<?php echo set_value('hid-desk');?>" name="hid-desk" />
                <input id="btn-submit-desk" class="light" type="submit" value="Send"/>
            </div>
            <?php
            if($success=='valid'){
                echo "
                    <div class='form-message'>
                        <p>Your request has been sent</p>
                        <p>Thank you !</p>
                    </div>
                ";
            }
            
            ?>
        </form>
    </header>
    <div id="contenu">
        <div class="btn-header el-mobile">
            <i id="btn-nav" class="i-nav material-icons">&#xe5d2;</i>
        </div>