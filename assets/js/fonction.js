$(document).ready(function(){
    $(function(){
                
        /*Nav page header.php desktop*/
        var position = 0;
        var contact = true;
        var hid_desk = $('#hid-desk').val();
        
        
        
        if(hid_desk=='submit')
        {
            $('#contact').addClass('contact-btn');
            $('#contact').removeClass('contact');
            contact = false;
            $('.form-content').slideToggle('slow');
        }
        
        if($('.form-message').length)
        {
            $('.form-message').css('display', 'block');
            $('.form-content').css('display', 'none');
        }
        
        
        $('#contact, #contact-content').on("click",
        function(){
            if(contact){
                $('#contact').addClass('contact-btn');
                $('#contact').removeClass('contact');
                contact = false;
            }else{
                $('#contact').addClass('contact');
                $('#contact').removeClass('contact-btn');
                contact = true;
            }
            $('.form-message').css('display', 'none');
            $('.form-content').slideToggle('slow');
        });
        
        $("#btn-submit-desk").on('click', function(){
            $('#hid-desk').val("submit");
        });
        
        
        
        
        /*Slide page atomik.php destop*/
        $("#btn-left-slide").css("color","#5c5c5c");
        
        $('#btn-left-slide').on("click",
        function(){
            if(position>=1){
                $("#f-slide").animate({ left : '+=60%'},"slow");
                $(".btn-slide").css("color","#1c1c1c");
                position--;
            }
            if(position==0){
                $("#btn-left-slide").css("color","#5c5c5c");
            }
            $('.slide-active').removeClass('slide-active');
            var dataSlide = '[data-slide="'+position+'"]';
            $(dataSlide).addClass('slide-active');
        });
        
        $('#btn-right-slide').on("click",
        function(){
            if(position<=3){
                $("#f-slide").animate({ left : '-=60%'},"slow");
                $(".btn-slide").css("color","#1c1c1c");
                position++;
            }
            if(position==4){
                $("#btn-right-slide").css("color","#5c5c5c");
            }
            $('.slide-active').removeClass('slide-active');
            var dataSlide = '[data-slide="'+position+'"]';
            $(dataSlide).addClass('slide-active');
        });
        
        
        /*Modal page atomik.php destop*/
        
        $('.i-gallery').on('click', function(){
            var dataModal = $(this).attr('data-modal');
            $('#img-modal').attr("src","../../assets/img/500/galerie-full-size/"+dataModal+".jpg");
            $('#modal').css('display','block');
        });
        
        $('#close').on('click', function(){
            $('#modal').css('display','none');
            $('#img-modal').empty();
        });
        var modal = document.getElementById('modal');
        
        window.onclick = function(event) {
            if (event.target == modal) {
                $('#modal').css('display','none');
            }
        };
        
        
        
        
        /*Nav header.php mobile*/
        var menu = true;
        var height = $(window).height();
        var hid_mobile = $('#hid-mobile').val();
        
        $(window).resize(function() {
            height = $(window).height();
        });
        
        var top = '-'+height;
        
        if(hid_mobile=="submit")
        {
            $('#contact-form-mobile').animate({ top : 0},"slow");
            $('#body-content').animate({ top: height },"slow");
        }
        
        $("#btn-submit-mobile").on('click', function(){
            $('#hid-mobile').val("submit");
        });
        
        $('#btn-nav').on("click",
        function(){
            if(menu){
                $('header').animate({ left: '0%'},"slow");
                $('#contenu').animate({ right: '-=100%'},"slow");
                $('body').css('height', height + 'px');
                menu = !menu;
            }
        });
        
        $('#btn-close').on("click",
        function(){
            if(!menu){
                $('header').animate({ left: '-=100%'},"slow");
                $('#contenu').animate({ right: '0%'},"slow");
                $('body').css('height', 'auto');
                menu = !menu;
            }
        });
        
        $('#contact-form-mobile').css('top', top+'px');
        
        $('.contact-mobile').on("click",
        function(){
            $('#contact-form-mobile').animate({ top : 0},"slow");
            $('#body-content').animate({ top: height },"slow");
        });
        
        $('#btn-close-contact').on("click",
        function(){
            $('#contact-form-mobile').animate({ top : top},"slow");
            $('#body-content').animate({ top: 0 },"slow");
            $('.form-message').css('display', 'none');
            $('.form-content').css('display', 'block');
        });
        
    });
});